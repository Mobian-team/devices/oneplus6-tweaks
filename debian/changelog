mobian-oneplus6-tweaks (0.2.8) unstable; urgency=medium

  * d/install: install modem setup service under `/lib/systemd`
    `debhelper` looks for service files under `/lib/systemd`, but ignores
    those installed under `/usr/lib/systemd`.

 -- Arnaud Ferraris <aferraris@debian.org>  Tue, 10 Jan 2023 23:34:06 +0100

mobian-oneplus6-tweaks (0.2.7) unstable; urgency=medium

  * tweaks: drop unneeded tweaks.
    Carrying the feedbackd profile is unnecessary as it's now part of an
    upstream release. Similarly, initramfs scripts are now part of
    mobile-initramfs-tools and can therefore be dropped.
    The latter, however, requires changing the cmdline parameters as the
    current version now uses mobile.root and mobile.r{o,w} instead of
    mobian.root and ro or rw. Updating the bootimg update script
    is therefore required to ensure the proper parameters are used.
  * d/control: depend on libqmi-utils and abootimg.
    Those are needed by the scripts provided by this package.
  * debian: add gbp.conf.

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Tue, 29 Mar 2022 14:56:30 +0200

mobian-oneplus6-tweaks (0.2.6) unstable; urgency=medium

  * zz-update-bootimg: exit when not running on device

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 03 Aug 2021 19:13:15 +0200

mobian-oneplus6-tweaks (0.2.5) unstable; urgency=medium

  [ Undef ]
  * d/salsa-ci: Add Debian's CI

  [ Arnaud Ferraris ]
  * zz-update-bootimg: check we're actually running on the device

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 03 Aug 2021 19:10:11 +0200

mobian-oneplus6-tweaks (0.2.4) unstable; urgency=medium

  * tweaks: update for 5.14 kernel changes
  * tweaks: add script for updating the boot image on kernel upgrade

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sun, 25 Jul 2021 19:48:12 +0200

mobian-oneplus6-tweaks (0.2.3) unstable; urgency=medium

  * tweaks: add feedbackd theme for the OnePlus 6T

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 18 May 2021 23:41:09 +0200

mobian-oneplus6-tweaks (0.2.2) unstable; urgency=medium

  * fix service install

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 27 Apr 2021 00:40:25 +0200

mobian-oneplus6-tweaks (0.2.1) unstable; urgency=medium

  * tweaks: improve feedbackd theme
  * initramfs-tools: fix partition resize and add modem firmware
  * tweaks: add service to configure modem on startup

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Mon, 26 Apr 2021 23:53:00 +0200

mobian-oneplus6-tweaks (0.2) unstable; urgency=medium

  * tweaks: add initramfs script for fixing the root partition/image

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Mon, 26 Apr 2021 19:09:37 +0200

mobian-oneplus6-tweaks (0.1) unstable; urgency=medium

  * Initial package release

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 24 Apr 2021 12:53:39 +0200
